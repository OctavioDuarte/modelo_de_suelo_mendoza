const fs = require('fs');
const papa = require('papaparse');

const data = JSON.parse( fs.readFileSync('./reflectometer_pilot.json') );
const lab_measurements = papa.parse( fs.readFileSync('./mendoza_laboratorio_2.csv', 'utf8'), { header: true } )
      .data
;

let mendoza_data = data.filter( d => d.data.sample_id.value?.match(/(regosh)|(soilab)/i) )
    .flatMap( submission => {
        let first_scan = submission
            .data
            .soil_intake
            .scan_dried_ground
            .x1
            .value
        ;
        if (first_scan) {
            first_scan.repetition_number = 1;
        };
        let second_scan = submission
            .data
            .soil_intake
            .scan_dried_ground
            .x2
            .value
        ;
        if (second_scan) {
            second_scan.repetition_number = 2;
        }
        ;
        let third_scan = submission
            .data
            .soil_intake
            .scan_dried_ground
            .x3
            .value
        ;
        if (third_scan) {
            third_scan.repetition_number = 3;
        };
        let output =  [
            first_scan,
            second_scan,
            third_scan
        ]
            .filter( d => d )
        ;
        let lab_data = lab_measurements
            .find( d => d.lab_id == submission.data.sample_id.value.toLowerCase() )
        ;


        output.forEach( entry => {
            delete entry.device_battery;
            delete entry.device_version;
            delete entry.device_firmware;
            delete entry.protocols;
            delete entry.calibration;
            delete entry.temperature;
            delete entry.humidity;
            delete entry.pressure;
            delete entry.voc;
            entry.lab_id = submission.data.sample_id.value.toLowerCase();
            if (lab_data) {
                Object.assign( entry, lab_data );
                entry.has_lab = true;
            } else {
                entry.has_lab = false;
            };
        } );
        return output;
    } )
;

let orphans = lab_measurements.filter( d => !mendoza_data.find( a => a.lab_id ==  d.lab_id ) )
;

mendoza_data.filter( d => d.lab_id.match(/25/) ).map( d => d["Carbono Total"] );

fs.writeFileSync('mendoza_soil_scans.json', JSON.stringify(mendoza_data));
fs.writeFileSync('mendoza_soil_scans.csv', papa.unparse(mendoza_data));
