const fs = require('fs');
const papa = require('papaparse');

let data = papa.parse( fs.readFileSync('./farmos_upload.csv', 'utf8'), {"header":true} )
  .data
  .map( row => {
      [ 'pH', 'Conductividad', 'Carbono Total', 'C/N' ].forEach( name => {
          if (row[name]) { row[name] =parseFloat( row[name].replace(',','.') ); }
      } );
      return row;
  }
  )
;


let targets = {
    lab_id: {section:[], name:'drupal_uid'},
    'Soil Texture': { section:['params', 'values'] , name: 'Soil Texture' },
    pH: {section: [ 'values' ], name: 'pH'},
    Conductividad: { section: [ 'values' ], name: 'Electrical Conductivity' },
    'Carbono Total': { section: [ 'values' ], name: 'Soil Carbon % (Walkly Black)' },
    'C/N': { section:[ 'values' ], name: 'TC:TN' },
    // Ubicacion: '',
    // 'Superficie cultivada total': '',
    // 'Sistema de cultivo/manejo': '',
    // 'Tipo de fertilización': '',
    'plant type': { section: [], name:'cropType' }
};

function parseRow( row, targets ) {
    let output = {};
    output.values = [];
    output.params = [];
    let targetNames = Object.keys( targets );
    targetNames.forEach( name => {
        let targetObj = targets[name];
        let data = row[name];
        if (data) {
            targetObj.section.forEach( section => {
                output[section].push( { name: targetObj.name, value: data } );
            } );
        }
        if ( targetObj.section.length == 0 ) {
            output[targetObj.name] = data;
        }
    } );
    return output;
};

let oneRow = parseRow(data[0], targets);


let coffeeshopTable = data.map( d => {
    let output = parseRow(d, targets);
    output.producer = { id: 'cooperativa-ayllu.farmos.net' };
    output.cropType = 'apple';
    output.events = [];
    output.title = output.drupal_uid;
    return output;
} )
.filter( d => d.drupal_uid )
;

fs.writeFileSync( './coffeeshopTable.json', JSON.stringify( coffeeshopTable ) );
