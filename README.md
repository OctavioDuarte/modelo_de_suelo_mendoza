# Primeros pasos Hacia un Modelo de Carbono en el Suelo para Mendoza

  En el marco de ReGOSH 2022, comenzamos a recabar muestras de suelo con el objetivo de entrenar un modelo predictivo para carbono en el suelo, basado en la información colorimétrica que brinda el colorímetro de 10 bandas desarrollado por OurSci.

  Actualmente se dispone de unas 60 muestras y el modelo no alcanzó una precisión suficiente para difundirlo para su uso pero parece mostrar factibilidad en caso de alcanzar una mayor masa de datos.
  
## Objetivos
  
  Lograr un modelo de carbono en el suelo que sea:

1. Lo bastante preciso como para aportar resultados de valor operativo para el productor.
2. Basado en datos lo bastante disponibles como para realizar predicciones en el campo. 
3. Que tenga un margen de error y confianza fáciles de exponer y explicar para un tomador de decisiones sin profundo conocimiento de estadística.
4. Que corra  en un dispositivo lo bastante accesible. Actualmente disponemos de servidores donde alojarlo, pero esto no es ideal para predicciones en el campo en zonas remotas. 
5. Que esté bien documentado y sea abierto, sincero acerca de su concepción, precisión y utilidad y cuyas herramientas de elaboración sean a la vez evidentes y mejorables por cualquier parte interesada requieriendo sólo los conocimientos indispensables para cada paso.

## Estado Actual

  Disponemos de 60 muestras escaneadas triplemente.
  Consideramos que con una mayor cantidad de datos, la precisión podría llegar a niveles productivos. 
  Las muestras tienen varias variables adicionales de interés, algunas son metadatos que podrían enriquecer un modelo compuesto como un Bosque Aleatorio, otras pueden ser nuevos objetivos de predicción (N por ejemplo).
  Nuestros mejores intentos alcanzan un ajuste con un $$R^2$$ de 0,48.
  Todavía no elegimos un tipo de modelo concreto. Estamos en una fase de exploración técnica.
  No elegimos dónde alojarlo, pero tenemos recursos disponbles al menos para comenzar una vez que sea lo bastante preciso. 
  
## Herramientas Desarrolladas

1. Guión (script) formateador de datos, que toma lo obtenido desde las tablas de Excell y SurveyStack y lo unifica de manera coherente.
2. Programa de preparación del modelo, que incluye:
3. Detección de atípicos. 
4. Acumulación (clusterización) para evaluar la calidad de las curvas. 
5. Gráficos ilustrativos en varias etapas del proyecto. 
6. Generación de un informe Rmardkown automáticamente. 
7. Entrenamiento y remuestreo de varios modelos.
  
## Desafíos Particulares

  No está demostrado que se pueda lograr un modelo lo bastante preciso para carbono en suelos basado en colorimetría. 
  El rango de variación de carbono en el suelo en Mendoza es muy bajo (ver distribución en el informe), por lo que el modelo debe tener gran precisión para distinguir entre los diversos suelos de la provincia. 
  No conocemos el margen de error de los métodos en uso en general, ni del laboratorio en particular. 
  No disponemos de una técnica para aparear estas mediciones de carbono con otras mediciones obtenidas por otras téchicas. En particular, tenemos una gran base de datos basada en LOI (pérdida por ignición). Disponer de este mecanismo podría servir para comunicar los modelos e incrementar la precisión.
  
## 
  
