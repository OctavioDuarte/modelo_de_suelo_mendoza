library(tidyverse)
library(randomForest)

## this function will detect factors were less than two values are available and allow to take action and prevent the run from breaking due to an error in the contrasts function. 
enoughLevelsInFactors <- function(table) {
  factorsList <- sapply( table, class ) %>% keep( . == "character" ) %>% names
  output <- tibble(
    factor = factorsList
  ) %>%
    mutate(
      levels = map_dbl( factor, ~ table %>% select( .x ) %>% na.omit %>% unique %>% nrow )
    )
  return( output )
}

regressionTree <- function( predictedVar , species = FALSE, dSet, df, verbose = FALSE, kFolds = 15, importance = FALSE, mtry = c(1:40) ) {
  if ( verbose ) {
    print( paste0( "training:", dSet ) )
  }
  explicative <- datasets[[dSet]]
  modelData <- df %>%
    select( .data[[ predictedVar ]] , !!explicative, id_sample, Type ) %>%
    na.omit

  if ( species != FALSE ) {
    modelData <- modelData %>%
      filter( Type == !!species ) %>%
      select( - Type )

  if (verbose) {
    print( paste( c( "species", species, "variable", predictedVar, "data length", nrow( modelData ) ), sep = " ," ) )
  }

  } else {
    modelData <- modelData %>%
      select( - Type )
  }

  faultyFactors <- enoughLevelsInFactors( modelData ) %>% filter( levels < 2 )

  ## assign( x="latestDataset", value=modelData, envir = globalenv() )
  if ( nrow( modelData ) < 50 | nrow( faultyFactors ) > 0 ) {
    if (verbose) {
      print( paste( "Nrow is", nrow(modelData) ) )
      print( paste( "faulty factors amount", nrow( faultyFactors ) ) )
      print( faultyFactors )
    }
    return( list( results= tribble( ~Rsquared, 0 ), N = 0, bestTune = list( mtry = 0 ), faultyFactors = faultyFactors ) )
  }

  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  modelFormula <- as.formula( c( predictedVar, "~", "." ) %>% str_c( collapse = "" ) )

  treeTGrid <- expand.grid(
    mtry = mtry
  )

  treeTCont <- trainControl( method="cv",
                            index = repetitionsFolds,
                            number = kFolds
                            )


  model <- train(
    modelFormula,
    data = modelData,
    method = "qrf",
    metric = "Rsquared",
    trControl = treeTCont,
    tuneGrid = treeTGrid,
    importance = importance,
    keep.inbag = TRUE
  )
  model$faultyFactors <- faultyFactors
  return(model)
}


classificationTree <- function( predictedVar, pType = FALSE, dSet, df, verbose = FALSE, kFolds = 15, mtry = c(1:40) ) {
  if ( verbose ) {
    print( paste0( "training:", dSet ) )
  }
  explicative <- datasets[[dSet]]
  modelData <- df %>%
    select( .data[[predictedVar]], !!explicative, id_sample, Type ) %>%
    na.omit

  if ( species != FALSE ) {
    modelData <- modelData %>%
      filter( Type == !!species ) %>%
      select( - Type )
  } else {
    modelData <- modelData %>%
      select( - Type )
  }

  modelData[[predictedVar]] <- fct_drop( modelData[[predictedVar]] )

  faultyFactors <- enoughLevelsInFactors( modelData ) %>% filter( levels < 2 )


  if ( nrow( modelData ) < 50 | nrow( faultyFactors ) > 0 ) { return( list( results= tribble( ~ Accuracy, 0 ) , N = 0, bestTune = list( mtry = 0 ), faultyFactors = faultyFactors ) ) }
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>% select( - id_sample )
  modelFormula <- as.formula( c( predictedVar, "~", "." ) %>% str_c( collapse = "" ) )

  if (verbose) {
    print( paste( c( "species", species, "variable", variable, "data length", nrow( modelData ) ), sep = " " ) )
  }

  treeTCont <- trainControl( method="cv",
                            index = repetitionsFolds,
                            number = kFolds
                            )
  treeTGrid <- expand.grid(
    mtry = mtry
  )
  model <- train(
    modelFormula,
    data = modelData,
    method = "rf",
    ## metric = "Accuracy",
    trControl = treeTCont,
    tuneGrid = treeTGrid,
    ntree = 500
  )
  model$faultyFactors <- faultyFactors 
  return(model)
}

crossValidateLM <- function( pType = FALSE, dSet, predictedVar, df, verbose = FALSE, kFolds = 15 ) {
  explicative <- datasets[[dSet]]

  if ( pType ) {
    df %>% filter( Type == pType )
  }

  modelData <- df %>%
    select( predictedVar, !!explicative, id_sample ) %>%
    na.omit
  repetitionsFolds <- groupKFold( group = modelData$id_sample, k = kFolds  )
  modelData <- modelData %>%
    select( - id_sample )

  if (verbose) {
    print(dSet)
  }
  modelFormula <- as.formula( c( predictedVar, "~", "." ) %>% str_c( collapse = "" ) )
  trainCont <- trainControl( method="cv",
                            number = 15,
                            index = repetitionsFolds,
                            )
  modelFormula <- paste0( predictedVar, "~" ,"." ) %>% as.formula
  model <- train(
    modelFormula,
    data = modelData,
    method = "lm",
    trControl = trainCont,
    tuneGrid = lmTGrid
  )
  return(model)
}

## These functions are devoted to get nice outputs from the Quantile Regression Random Forest model, allowing to plot the intervals and easily get all the relevant metrics.
## Currently, it attempts two levels, 20% first and if the non cover rate is too hight, 10%.

## Quantile names are by default really uncomfortable to work with, so we remove empty spaces, points and the = sign from the name, so `quantile= 0.5` -> centile5
sanitizeQuantilesNames <- function( tibble ) {
  tibble %>%
    rename_at(
      vars( contains("quantile") ),
      .funs = ~ str_c( "centile",
                      as.double( str_extract( .x, pattern = "0[:punct:]{1}[:alnum:]*" ) ) * 100
                      )
    )
}

## takes the training object from Caret's train object of a quantile regression random forest
## caclulate the observed cover percentage and median interval width to range ratio.
## get a table that simplifies ploting the intervals.
getIntervalsTable <- function( model, predictedName ) {

  trainingData <- model$trainingData %>%
    rename( !!sym(predictedName) := `.outcome` )

  varRange <- trainingData[[predictedName]] %>% sort 
  varRangeTrimmed <-  varRange[ c( 1:ceiling( 0.975 * length(varRange) ) ) ]
  predictedMax <- max( varRangeTrimmed, na.rm = TRUE )
  predictedMin <- min( varRangeTrimmed, na.rm = TRUE )
  predictedRange <- predictedMax - predictedMin

  ## predictedMax <- max( trainingData[[predictedName]], na.rm = TRUE )
  ## predictedMin <- min( trainingData[[predictedName]], na.rm = TRUE )
  ## predictedRange <- predictedMax - predictedMin

  intervals <- predict( model$finalModel,
                       newdata =  NULL,
                       what = c( 0.05, 0.1, 0.9, 0.95 ),
                       all = FALSE,
                       obs = 1 ) %>%
    as_tibble() %>%
    sanitizeQuantilesNames %>%
    mutate(
      width10 = centile95 - centile5,
      width20 = centile90 - centile10,
      relative_width_10 = width10 / predictedRange,
      relative_width_20 = width20 / predictedRange,
      !!sym(predictedName) := model$finalModel$y,
      y_hat = model$finalModel$predicted,
      covers10 = ifelse( .data[[predictedName]] < centile5 | .data[[predictedName]] > centile95, FALSE, TRUE ),
      covers20 = ifelse( .data[[predictedName]] < centile10 | .data[[predictedName]] > centile90, FALSE, TRUE )
    )

  medianWidthToRangeRatio10 <- median( intervals$width10 ) / predictedRange
  medianWidthToRangeRatio20 <- median( intervals$width20 ) / predictedRange
  coverPercentage10 <- mean( intervals$covers10 )
  coverPercentage20 <- mean( intervals$covers20 )

  output <- list()

  if ( coverPercentage20 < 0.7 ) {
    intervals <- intervals %>%
      mutate(
        intervalCeiling = centile95,
        intervalFloor = centile5,
        width = width10,
        covers = covers10,
        relative_width = relative_width_10
          )
    output$intervalLevel <- 10
    output$medianWidthToRangeRatio <- medianWidthToRangeRatio10
    output$observedCoverPercentage <- coverPercentage10
      } else {
        intervals <- intervals %>%
          mutate(
            intervalCeiling = centile90,
            intervalFloor = centile10,
            width = width20,
            covers = covers20,
            relative_width = relative_width_20
          )
        output$intervalLevel <- 20
        output$medianWidthToRangeRatio <- medianWidthToRangeRatio20
        output$observedCoverPercentage <- coverPercentage20
      }

  output$table <- intervals %>% rename( y = .data[[predictedName]] )

  output$ecdf <- ecdf( output$table$y )

  return(output)
}

##  get legacy non robust widths to compare with previous briefs
## see getIntervalsTable
getIntervalsTableLegacy <- function( model, predictedName ) {

  trainingData <- model$trainingData %>%
    rename( !!sym(predictedName) := `.outcome` )

  varRange <- trainingData[[predictedName]] %>% sort 
  predictedMax <- max( varRange, na.rm = TRUE )
  predictedMin <- min( varRange, na.rm = TRUE )
  predictedRange <- predictedMax - predictedMin

  ## predictedMax <- max( trainingData[[predictedName]], na.rm = TRUE )
  ## predictedMin <- min( trainingData[[predictedName]], na.rm = TRUE )
  ## predictedRange <- predictedMax - predictedMin

  intervals <- predict( model$finalModel,
                       newdata =  NULL,
                       what = c( 0.05, 0.1, 0.9, 0.95 ),
                       all = FALSE,
                       obs = 1 ) %>%
    as_tibble() %>%
    sanitizeQuantilesNames %>%
    mutate(
      width10 = centile95 - centile5,
      width20 = centile90 - centile10,
      relative_width_10 = width10 / predictedRange,
      relative_width_20 = width20 / predictedRange,
      !!sym(predictedName) := model$finalModel$y,
      y_hat = model$finalModel$predicted,
      covers10 = ifelse( .data[[predictedName]] < centile5 | .data[[predictedName]] > centile95, FALSE, TRUE ),
      covers20 = ifelse( .data[[predictedName]] < centile10 | .data[[predictedName]] > centile90, FALSE, TRUE )
    )

  medianWidthToRangeRatio10 <- median( intervals$width10 ) / predictedRange
  medianWidthToRangeRatio20 <- median( intervals$width20 ) / predictedRange
  coverPercentage10 <- mean( intervals$covers10 )
  coverPercentage20 <- mean( intervals$covers20 )

  output <- list()

  if ( coverPercentage20 < 0.7 ) {
    intervals <- intervals %>%
      mutate(
        intervalCeiling = centile95,
        intervalFloor = centile5,
        width = width10,
        covers = covers10,
        relative_width = relative_width_10
          )
    output$intervalLevel <- 10
    output$medianWidthToRangeRatio <- medianWidthToRangeRatio10
    output$observedCoverPercentage <- coverPercentage10
      } else {
        intervals <- intervals %>%
          mutate(
            intervalCeiling = centile90,
            intervalFloor = centile10,
            width = width20,
            covers = covers20,
            relative_width = relative_width_20
          )
        output$intervalLevel <- 20
        output$medianWidthToRangeRatio <- medianWidthToRangeRatio20
        output$observedCoverPercentage <- coverPercentage20
      }

  output$table <- intervals %>% rename( y = .data[[predictedName]] )

  output$ecdf <- ecdf( output$table$y )

  return(output)
}
