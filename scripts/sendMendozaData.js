const mongo = require('mongodb');
const fs = require('fs');
require('dotenv').config( { path: '../.env' } );
var connectionAtlas = process.env.MONGO_URI;

/**
 * UPloads objects in the data array to a collection in a chose server.
 * @param {} dashboards
 * @param {} collectionName
 * @param {} data
 * @param {} connectionLocal
 * @returns {} 
 */
async function commitArbitraryCollection( collectionDb='dashboards', collectionName, data, connectionString = connectionLocal ) {

    const clientLocal = new mongo.MongoClient(connectionString, { useUnifiedTopology: true });
    let results;
    try {
        await clientLocal.connect();
        results = await clientLocal
            .db(collectionDb)
            .collection(collectionName)
            .insertMany(data,
                        { ordered: false }
                       )
        ;
        console.log(`${results.insertedCount} new listing(s) created with the following id(s):`);
        // console.log(result.insertedIds);
    } catch (e) {
        console.error(e);
    } finally {
        await clientLocal.close();
        return results;
    }
    ;
};
